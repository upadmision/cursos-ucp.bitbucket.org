<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cursos UCP</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="http://universidaddelascompetencias.pe/home/sites/default/files/favicon_ultimo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/custom.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img src="img/logo-ucp.png" alt="">
				</div>
				<div class="col-md-3">
					<img src="img/logo-unesco.png" alt="">
				</div>
				<div class="col-md-4">
					<img src="img/logo-inventory.png" alt="">
				</div>
			</div>
		</div>
	</header>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h2 class="title-ucp">Cursos y <br><span>Especializaciones</span></h2>
				</div>
				<div class="col-md-3">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation">
							<a href="#home" aria-controls="home" role="tab" data-toggle="tab">CURSOS</a>
						</li>
						<li role="presentation" class="active">
							<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">CONTÁCTANOS</a>
						</li>
					</ul>
				</div>

				<div class="col-md-12">
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in" id="home">

							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<h2 class="title-tab">Área de Ciencias Empresariales</h2>
								<!-- Collapse 1 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												Curso de Gestión de Proyectos
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<p>
												Obtendrás conocimientos prácticos, actualizados y totalmente aplicables a la realidad de las necesidades en gestión de proyectos de las organizaciones. Conocerás en profundidad los métodos, sistemas y herramientas relacionados con la Dirección de Proyectos. Al adquirir los conocimientos en dirección de proyectos conforme a estándares internacionales de reconocido prestigio: Project Management Institute (PMI).
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-de-ciencias-empresariales/curso-gestion-proyectos.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/prueba.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>
											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 2 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwo">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
												Curso de Técnicas y Herramientas de Mejora Continua en la Gestión de Procesos de la Calidad
											</a>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
										<div class="panel-body">
											<p>
												Conocerás los distintos niveles de documentos que configuran un Sistema de Gestión de la Calidad. Podrás estudiar la gestión por Procesos: norma UNE-EN ISO 9001 y el Modelo Europeo de Excelencia de la Calidad EFQM. Manejaras el concepto de mejora continua y las herramientas de calidad más importantes.
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-de-ciencias-empresariales/tecnicas-y-herramientas-de-mejora-continua-en-la-gestion-de-pde-calidad.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/prueba.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>
											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 3 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
												Curso de Auditor Interno en Calidad ISO 9001
											</a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										<div class="panel-body">
											<p>
												Podrás analizar todas las fases de una auditoría de calidad, conocer la norma ISO 9001:2008, establecer las pautas para la correcta programación de una auditoría completar informes de auditoría ajustado a las normas ISO 9001 y estudiar las acciones de seguimiento necesarias tras una auditoría.
											</p>
											<p class="text-right">
												<a class="green-txt" href="pdf/area-de-ciencias-empresariales/curso-auditor-interno-calidad-iso-9001.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/prueba.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>
											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 4 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFour">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
												Técnico en Dirección Logística y de la Cadena de Suministro
											</a>
										</h4>
									</div>
									<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
										<div class="panel-body">
											<p>
												Tendrás dominio de tecnologías de la información logística, conocerás el Soporte Logístico Integrado y todo lo relacionada con los sistemas y la ingeniería logística.
											</p>
											<p class="text-right">
												<a class="green-txt" href="pdf/prueba.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>
											</p>
										</div>
									</div>
								</div>

								<!-- Collapse 5 -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingfive">
										<h4 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
												 Curso de Las Tecnologías de la Información en Logística
											</a>
										</h4>
									</div>
									<div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
										<div class="panel-body">
											<p>
												La logística es el conjunto de acciones y medios destinados a proveer los recursos necesarios para realizar una actividad en un tiempo, forma y al menor coste en un marco de productividad y calidad. Actualmente, la logística es un campo esencial en el funcionamiento de toda empresa para posicionarse a nivel global. En las últimas décadas, su planteamiento ha evolucionado y sus complejidad.
											</p>
											<p class="text-right">

												<a class="green-txt" href="pdf/area-de-ciencias-empresariales/las-tecnologias-de-la-informacion-en-logistica.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

												<a class="green-txt" href="pdf/prueba.pdf" target="_blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Descargar Programa</a>

											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade in active" id="profile">
							<div class="row">
								<div class="col-md-5">
									<div class="row">
										<div class="col-md-6 formulario-ucp">
											<form action="" class="form-ucp">
												<h4>Tú pones el horario, nosotros la experiencia </h4>
												<h3>¡Dale un nuevo impulso a tu carrera!</h3>
												<div class="form-group">
													<!-- El nombre lo tengo que sacar de acá -->
													<input type="text" required class="form-control" id="fieldName" placeholder="Nombre y Apellido" name="Nombre">
												</div>

												<div class="form-group">
													<input type="email" required class="form-control" id="fieldMail" placeholder="E-mail" name="Email">
												</div>

												<div class="form-group">
													<input type="text" required class="form-control" id="fieldPhone" placeholder="Teléfono" name="Telefono">
												</div>

												<div class="form-group">
													<select required class="form-control" name="Curso" id="">
														<option value="">--Seleccionar curso--</option>
														<option value="1">Curso de Gestión de Proyectos</option>
														<option value="2">Curso de Técnicas y Herramientas de Mejora Continua en la Gestión de Procesos de la Calidad</option>
														<option value="3">Curso de Auditor Interno en Calidad ISO 9001</option>
														<option value="4">Técnico en Dirección Logística y de la Cadena de Suministro</option>
														<option value="5">Curso de Las Tecnologías de la Información en Logística</option>
													</select>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox"> 
														<a href="#" data-toggle="modal" class="link green-txt" data-target="#terminos">Terminos y condiciones</a>
													</label>
												</div>
												
												<hr>
												<button id="botonEnviar" class="btn btn-green" href="#" role="button">Solicitar información</button>
											</form>
										</div>
									</div>

								</div>
								<div class="col-md-4 col-md-offset-3"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Modal -->
	<div class="modal fade" id="terminos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Términos y Condiciones</h4>
	      </div>
	      <div class="modal-body">
	      	
			<h2>POLITICA DE PRIVACIDAD Y DE PROTECCION DE DATOS PERSONALES</h2>
			<p><strong>“TELEFÓNICA LEARNING SERVICES PERÚ S.A.C.”</strong>,  en adelante, <strong>TLS</strong>, es el titular del banco de datos personales en el cual se recopilará todos los datos personales brindados en el formulario del Registro “Contacto”, así como de los datos personales que facilite de manera directa o indirecta.</p>
			 
			<p>Mediante la aceptación de esta política de privacidad y de protección de datos personales, Ud. acepta y consiente de manera expresa a TLS tratar los datos personales que proporcione para los siguientes fines: responder consultas acerca de productos y servicios, atención de reclamos, para el cumplimiento de obligaciones ante una eventual obligación contractual entre TLS y el usuario, para el envío de publicidad mediante cualquier medio y soporte, envío de invitaciones a actividades convocadas por TLS o sus socios comerciales y para fines estadísticos. Todos los campos de registro son de llenado obligatorio, sin el llenado de los campos obligatorios no se podrá realizar el envío de la consulta.</p>
			 
			<p>El TLS asegura tratar sus datos personales cumpliendo diligentemente las obligaciones establecidas en la Ley 29733, Ley de protección de datos personales y demás normas complementarias, en especial las referidas a la guarda de confidencialidad de la información y aplicación de las medidas de seguridad pertinentes.</p>
			 
			<p>El titular del dato personal o su representante podrá presentar la solicitud de ejercicio de sus derechos reconocidos en la Ley 29733 utilizando alguno de los siguientes canales de comunicación:</p>

			<p>Escribiendo a cau@tedsoporte o a la siguiente dirección: Av. Camino Real 155 Piso 4 – San Isidro</p>
			<p>Llamando al teléfono 2224760 en horario de Lunes a Viernes de 09:00 am a 05:30 pm.</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
	      </div>
	    </div>
	  </div>
	</div>
	<footer id="socios-ucp">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<h4>Socios estratégicos</h4>
				</div>
				<div class="col-md-2"><img src="img/logo-imf.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-senati.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-cvn.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-upb.png" alt=""></div>
				<div class="col-md-2"><img src="img/logo-cayetano.png" alt=""></div>
			</div>
		</div>
	</footer>
	<footer id="powered-by-ted">
		<div class="container">
			<div class="row">
				<div class="col-md-10 text-right">
					<p>Powered by:</p>
				</div>
				<div class="col-md-2 text-left">
					<img src="img/logo-ted.png" alt="">
				</div>
			</div>
		</div>
	</footer>
	<div class="modal fade" id="message-send" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-body">
	      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<!-- Tengo que imprimir el nombre del usario acá -->
			<p id="name-user">Estimado Usuario</p>

	      	<h3>¡Muchas gracias por registrarte!</h3>

			<p>En breve nos comunicaremos contigo.</p>

			<a href="http://universidaddelascompetencias.pe/home/" class="btn-green padding-10">Visítanos en nuestra Web</a>
	      </div>
	    </div>
	  </div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		(function($){
			$(document).ready(function(){
				$('#someTab').tab('show');
				$('.form-ucp').on('submit',function(e){
					e.preventDefault();
					var $this = $(this),
						data = $this.serialize();
						$this.find('input[type="text"]').val('');
						$this.find('input[type=email]').val('');
						$this.find('select').val('');
						$this.find('input[type="checkbox"]').prop('checked',false);
						
						$.ajax({
							url:'enviar.php',
							method:'post',
							data:data,
							success:function(){
								$('#message-send').modal();
								//$("#name-user").html('Hola, '+$("#fieldName").val());
							}
						})
				});	
			});

		})(jQuery);
	</script>
</body>
</html>
